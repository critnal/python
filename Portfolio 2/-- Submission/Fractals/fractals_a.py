
########## BEGIN DESCRIPTION ############################################
##
##  Fractal Drawing.
##  This program draws various fractal images using the concepts of
##  L-Systems and with Turtle Graphics.
##
########## END DESCRIPTION ##############################################



########## BEGIN PAIR DETAILS #####################################
##
##  Student Name:  Sean Seeto
##
##  Student Number:  n8324018
##
##  Student Name:  Alex Crichton
##
##  Student Number:  n6878296
##
########## END PAIR DETAILS #######################################



########## BEGIN MARKER'S FEEDBACK ###################################
##
##
########## END MARKER'S FEEDBACK #####################################




########## BEGIN Fractal Code ###########################
##
##
##  Algorithm:
##
##  *** PUT YOUR ALGORITHM HERE ***

##  1st. Define the fractal parameters

##  2nd. Check number of targets/replacements

##  3rd. Replace every instance of target with its replacement

##  4th. Repeat for designated number of times

##  5th. Direct Turtle with commands based on the result





##  Program code:
##
##  *** PUT YOUR PROGRAM CODE BELOW ***






# Import the turtle module
from turtle import *





def draw_fractal(initial_state, target, replacement, \
                 num_replacements, length, angle):
    """
    This function draws a fractal using the following parameters:
       initial_state is a string
       'target' is the command to be replaced with 'replacement'
       num_replacements is an integer indicating how many times to
          the replacement rules
       length is the length to go forward
       angle is the angle to turn (either right or left)
     """

    # Set speed to fastest
    speed('fastest')

    # Remove the tracer delay
    tracer(0)



    # Create the final string with commands for turtle 
    # Execute this loop for the designated number of replacements
    for count in range(num_replacements) :

        # If there is only only target string
        if not isinstance(target, list) :

            # Replace every instance of target with replacement
            initial_state = initial_state.replace(target, replacement)

        # If multiple target strings
        else :

            # Iterate over every index in the list of targets/replacements
            for index in range(len(target)):
                initial_state = initial_state.replace(target[index], replacement[index])
            
                       

    # For every character in string execute turtle command
    for character in initial_state :

        # If character is 'F' go forward with pen down
        if character == 'F' :

            down()
            forward(length)

        # If character is 'f' go forward without drawing
        elif character == 'f' :

            up()
            forward(length)

        # If character is '-' turn left
        elif character == '-' :
            left(180 - angle)

        # IF character is '+' turn right
        else :      
            right(180 - angle)

        



        
##
########## END Fractal Code #############################



########## BEGIN AUTOMATIC DRAWING ###################################
##
##  The following code when uncommented will run each fractal drawing
## function
##
if __name__ == '__main__':
   
    ## Draws the Dragon_curve
##    draw_fractal('F', 'F', 'F+F-F', 7, 5, 60)

    ## Draws the Koch cube
##    draw_fractal('F-F-F-F', 'F', 'FF-F-F-F-FF', 3, 3, 90)

    ## Draws the Koch ring
##    draw_fractal('F-F-F-F', 'F', 'FF-F-F-F-F-F+F', 4, 3, 90)
    
    ## Draws the Island Lakes
##    draw_fractal('F+F+F+F', ['F', 'f'], ['Ff+f-FF+F+FF+Ff+FF-f+FF-F-FF-Ff-FFF', 'ffff'], 2, 3, 90)
##
########## END AUTOMATIC TESTING #####################################
