
########## BEGIN DESCRIPTION ############################################
##
##  Image Alteration.
##  This program manipulates a digital picture.
##  1.  The negative of a black and white bitmap is created.
##
##  Challenges
##  2.  A greyscale version of a colour jpeg is created.
##  3.  The negative of a colour jpeg is created.
##  4.  A lightened/darkened version of a colour jpg is created
##  5.  A tinged (red, green or blue) version of a colour jpg is created
##
########## END DESCRIPTION ##############################################



########## BEGIN PAIR DETAILS #####################################
##
##  Student Name:  Sean Seeto
##
##  Student Number:  n8324018
##
##  Student Name:  Alex Crichton
##
##  Student Number:  n6878296
##
########## END PAIR DETAILS #######################################



########## BEGIN MARKER'S FEEDBACK ###################################
##
##
########## END MARKER'S FEEDBACK #####################################




########## BEGIN Image Alteration Code ###########################
##  Program code:
##
##  *** PUT YOUR PROGRAM CODE BELOW ***

# Creates a black and white negative version of the provided bitmap
def bw_negative(filename):
    """
    This function creates a black and white negative of a bitmap image
    using the following parameters:
       filename is the name of the bitmap image
    """

    # Load the image into a callable variable
    image = Image.open(filename)

    # Load the pixel data from the image into a list
    pixels = list(image.getdata())

    # Create empty list for storing modified pixels into
    pixels_new = []



    # For every pixel in the list 'pixels'
    # invert the colour
    for pixel in pixels:

        # Invert pixel colour in the scale 0 - 255 (black - white)
        pixels_new.append(255 - pixel)



    # Store new pixel data into the 'image' variable
    image.putdata(pixels_new)

    # Generate a filename for the new image
    filename_new = filename[:-4] + ' negative.bmp'

    # Save the new image using the generated filename
    # This new image contains the modified pixel data
    image.save(filename_new)





# Creates a greyscale version of the provided jpeg
def greyscale(filename):
    """
    This function creates a greyscale jpeg version of a colour jpeg
    using the following parameters:
       filename is the name of the jpeg image
    """
    
    # Load the image into a callable variable
    image = Image.open(filename)

    # Load the pixel data from the image into a list
    pixels = list(image.getdata())

    # Create empty list for storing modified pixels into
    pixels_new = []



    # For every pixel in the list 'pixels'
    # retrieve the RGB values from the pixel data
    # modify them and store them in a single channel
    for pixel in pixels:

        # Each pixel is a tuple with 3 values, accessible with indexes
        red   = pixel[0]
        green = pixel[1]
        blue  = pixel[2]

        # Modify the RGB values
        # Add them together to create a single value
        # note: must be an integer
        pixels_new.append(int(red * 0.299 + green * 0.587 + blue * 0.114))

        # we are creating a greyscale image
        # the original image is not greyscale, it is an RGB image
        # RGB images have 3 channels, but greyscale images only have 1 channel
        # to make a greyscale image the 3 RGB channels have to be
        # converted to a single channel



    # Create a new image
    # This image is greyscale, indicated by the first parameter 'L'
    image_new = Image.new('L', image.size)

    # Store the new pixel data into the new image
    image_new.putdata(pixels_new)

    # Generate a filename for the new greyscale image
    filename_new = filename[:-4] + ' greyscale.jpg'

    # Save the new greyscale image using the generated filename
    # This new image contains greyscale pixel data
    image_new.save(filename_new)


    

        
# Creates a colour negative version of the provided jpeg
def colour_negative(filename):
    """
    This function creates a colour negative of a jpeg image
    using the following parameters:
       filename is the name of the jpeg image
    """


    # Load the image into a callable variable
    image = Image.open(filename)

    # Load the pixel data from the image into a list
    pixels = list(image.getdata())

    # Create empty list for storing modified pixels into
    pixels_new = []



    # For every pixel in the list 'pixels'
    # retrieve the RGB values from the pixel data
    # and invert them
    for pixel in pixels:

        # Each pixel is a tuple with 3 values, accessible with indexes
        red = pixel[0]
        green = pixel[1]
        blue = pixel[2]

        # Invert each of the pixel colours in the scale 0 - 255
        pixels_new.append((255 - red, 255 - green, 255 - blue))



    # Store the modified pixel data
    image.putdata(pixels_new)

    # Generate a filename for the new image
    filename_new = filename[:-4] + ' inverted.jpg'

    # Save the new image using the generated filename
    # This new image contains the modified pixel data
    image.save(filename_new)





# Creates a version of the provided jpeg with the brightness altered
# PARAMS:   filename = the original colour image file
#           action = {'lighten', 'darken'}
#           extent = percentage change required (numeric value between 0 and 100)
def change_brightness(filename, action, extent):
    """
    This function either increases or decreases the brightness of an image
    by altering each pixel in each band
    
    """

    # Load the image into a callable variable
    image = Image.open(filename)

    # Load the pixel data from the image into a list
    pixels = list(image.getdata())

    # Create empty list for storing modified pixels into
    pixels_new = []


    
    # If the action is to lighten
    # the extent remains unchanged
    # However, if the action is to darken
    # the extent becomes negative
    if action == 'darken' :
        extent *= -1
    


    # For each pixel in the list 'pixels'
    # Modify each of the RGB values by the desired 'extent'
    for pixel in pixels:

        # For each value of each pixel
        # Change the value by the 'extent'
        red   = pixel[0] + pixel[0] * extent / 100
        green = pixel[1] + pixel[1] * extent / 100
        blue  = pixel[2] + pixel[2] * extent / 100

        # note: the values must remain between 0 and 255
        if red   > 255 :
            red   = 255
        if green > 255 :
            green = 255
        if blue  > 255 :
            blue  = 255

        # Store the modified RGB values in the new list
        pixels_new.append((red, green, blue))



    # Store the modified pixel data
    image.putdata(pixels_new)

    # Generate a filename for the new image
    filename_new = filename[:-4] + ' ' + action + str(extent) + '.jpg'

    # Save the new image using the generated filename
    # This new image contains the modified pixel data
    image.save(filename_new)





# Create a version of the provided jpeg with a colour tinge
# PARAMS:   filename = the original colour image file
#           tinge = {'r', 'g', 'b'}

def tinge(filename, colour):
    """
    This function creates a new image tinged with the given colour
   
    """

    
    # Load the image into a callable variable
    image = Image.open(filename)

    # Load the pixel data from the image into a list
    pixels = list(image.getdata())

    # Create empty list for storing modified pixels into
    pixels_new = []


    
    # This section modifies either the R, G or B value of every pixel
    # depending on the 'tinge' parameter
    # The other channels remain unchanged

    # These are the initial modification values
    # They are set to zero until the function
    # tells one of the channels that it needs to be modified
    (mod_red, mod_green, mod_blue) = (0, 0, 0)

    # Now check the tinge parameter for the desired tinge
    # and set the appropriate modification value

    # If the tinge should be red
    if   colour == 'r' :
        # add 100 to every red value
        mod_red   = 100
        
    # If the tinge should be green
    elif colour == 'g' :
        # add 100 to every green value
        mod_green = 100
        
    # If the tinge should be blue
    else :
        # add 100 to every blue value
        mod_blue  = 100
        


    # For every pixel in the list 'pixels'
    for pixel in pixels:

        # apply the modification value to each channel
        # The channel will remain unchanged if its modification value
        # has remained zero
        red   = pixel[0] + mod_red
        green = pixel[1] + mod_green
        blue  = pixel[2] + mod_blue

        # note: RGB values must remain between 0 and 255
        if red   > 255 :
            red   = 255
        if green > 255 :
            green = 255
        if blue  > 255 :
            blue  = 255

        # Store the modified RGB values in the new list 
        pixels_new.append((red, green, blue))



    # Store the modified pixel data
    image.putdata(pixels_new)

    # Generate a filename for the new image
    filename_new = filename[:-4] + ' ' + colour + '.jpg'

    # Save the new image using the generated filename
    # This new image contains the modified pixel data
    image.save(filename_new)



    
        
##
########## END Image Alteration Code #############################



########## BEGIN AUTOMATIC CONVERSION ###################################
##
##  The following code when uncommented will run each conversion
## function
##
if __name__ == '__main__':
   
    from PIL import Image

##    ## Creates the black and white negative bitmap
##    bw_negative('farmland.bmp')
##
##    ## Creates the greyscale version of a colour jpeg
##    greyscale('balloon.jpg')
##
##    ## Creates a colour negative version of a colour jpeg
##    colour_negative('balloon.jpg')
##
##    ## Creates a version with brightness changed
##    change_brightness('balloon.jpg', 'darken', 50)
##
##    ## Creates a version tinged in a given colour
##    tinge('balloon.jpg', 'r')

##
########## END AUTOMATIC CONVERSION #####################################
