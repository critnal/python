
########## BEGIN DESCRIPTION ############################################
##
##  Popularity Cloud.
##  This program queries a MySQL database to extract the frequencies (count) 
##  of each value in a series of categories.
##
##  The extracted data is output to a HTML "popularity" cloud, which displays
##  values in a category which have the same frequency in the same colour
##  and text size, and those which are more frequent in a larger text size.
##  Each frequency is shown in a distinct text size.
##
########## END DESCRIPTION ##############################################



########## BEGIN PAIR DETAILS #####################################
##
##  Student Name:  *** WRITE YOUR NAME HERE***
##
##  Student Number:  *** WRITE YOUR STUDENT NUMBER HERE ***
##
##  Student Name:  *** WRITE YOUR NAME HERE***
##
##  Student Number:  *** WRITE YOUR STUDENT NUMBER HERE ***
##
########## END PAIR DETAILS #######################################



########## BEGIN MARKER'S FEEDBACK ###################################
##
##
########## END MARKER'S FEEDBACK #####################################


########## BEGIN Popularity Code ###########################
##
##
##  Program code:
##
##  *** PUT YOUR PROGRAM CODE BELOW ***

def create_clouds(categories):
    '''
    categories - a list of strings containing the names of each of the
    categories to create HTML files for
    '''

    import MySQLdb

    connection = MySQLdb.connect(host = "localhost", user = "root", \
                 passwd = "root", db = "airline")














































##
########## END Popularity Cloud Code #############################

########## BEGIN AUTOMATIC START ###################################
##
##  The following code will automatically run the given function
##  when the program is "run".  Do not change anything in this
##  section.  If you want to prevent it from running, comment
##  out the code below, but ensure that the code is uncommented when
##  you submit your program
##

# Run the function if this is the main program:
if __name__ == '__main__':
    create_clouds(['actors', 'movies', 'tv', 'sports', 'games', \
    'activities', 'musicians', 'books'])
##
########## END AUTOMATIC START #####################################

