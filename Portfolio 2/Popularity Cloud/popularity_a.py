
########## BEGIN DESCRIPTION ############################################
##
##  Popularity Cloud.
##  This program queries a MySQL database to extract the frequencies (count) 
##  of each value in a series of categories.
##
##  The extracted data is output to a HTML "popularity" cloud, which displays
##  values in a category which have the same frequency in the same colour
##  and text size, and those which are more frequent in a larger text size.
##  Each frequency is shown in a distinct text size.
##
########## END DESCRIPTION ##############################################



########## BEGIN PAIR DETAILS #####################################
##
##  Student Name:  Sean Seeto
##
##  Student Number:  n8324018
##
##  Student Name:  Alex Crichton
##
##  Student Number:  n6878296
##
########## END PAIR DETAILS #######################################



########## BEGIN MARKER'S FEEDBACK ###################################
##
##
########## END MARKER'S FEEDBACK #####################################


########## BEGIN Popularity Code ###########################
##
##
##  Program code:
##
##  *** PUT YOUR PROGRAM CODE BELOW ***



def create_clouds(categories):
    '''
    categories - a list of strings containing the names of each of the
    categories to create HTML files for
    '''

    import MySQLdb

    # Declare these global variables for use in multiple functions later
    # These variables can now be called in any function without the
    # need to insert them as parameters
    global output_file, value_list, count_list, color_list

    # List of colours to be applied to the tags (dependent on popularity)
    color_list = [('rgb(160,170,180)'), ('rgb(119,136,153)'), \
                  ('rgb(100,115,125)'), ('rgb(49,79,79)'), ('rgb(0,0,0)')]

    

    # Create a connection using relevant values for the server name, username,
    # password and database name
    connection = MySQLdb.connect(host = "localhost", user = "root",\
    passwd = "", db = "inb104")
    
    # Get a cursor on the database. This allows you to execute SQL queries and
    # get the results.
    cursor = connection.cursor()



    # For each of the categories entered into the create_clouds function
    for category in categories:   
            
        # Execute a SQL query
        cursor.execute("SELECT category, value, count(*) \
                        FROM popularity \
                        WHERE category = '" + category + "'\
                        GROUP BY value \
                        ORDER BY value")
        
        # Each row is a list of the column values from the database.
        rows = cursor.fetchall()

        # Ensure the global variables value_list and count_list are empty lists
        value_list = []
        count_list = []
        
        # For every row of data retrieved via the SQL query
        for row in rows:

            # Add the value and its count to their respective list 
            value_list.append(row[1])
            count_list.append(row[2])
            


        # Create a HTML page for the current category
        # The filename will be [current category].html
        # The file will be opened in Write mode
        output_file = open(category + '.html', 'w')



        # Write the first section of the html code to the file
        # This section (except the title) is static
        write_head(category)    # The category parameter will be the title
                                # of the html page

        # Write a tag for every value in value_list
        for index in range(len(value_list)):
            write_tag(index)    # Paramater 'index' will tell the function
                                # the value it is currently writing
                                
        # Write the final section of the html code to the file
        # This section is static
        write_foot()

        # Close the file
        output_file.close()

            
    
    # Close the cursor
    cursor.close()
    
    # Close the database connection
    connection.close()
    



                  
# Write the first section of the html code to the file
# This section (except for the title) is static                      
def write_head(title):
    
    output_file.write('<html>')
    output_file.write('<head>')
    output_file.write('<title>')
    output_file.write(title)
    output_file.write('</title>')
    output_file.write('</head>')

    # Set the page's font to Times New Roman
    output_file.write('<body style="font-family:times">')

    # Set up a table to contain the popularity cloud
    output_file.write('<table border="1" bordercolor="rgb(49,79,79)" \
                       cellpadding="2"><tr><td>')
    




# Write the final section of the html code to the file
# This section is static     
def write_foot():

    # Retrieve global function
    global color_list

    # End table section
    output_file.write('</td></tr></table>')

    # Begin footnote section
    output_file.write('<p style="text-align:center">')
    output_file.write('<span style="color:' + \
                        color_list[0] + \
                      ';font-size:10">This style is for count = 1</span><br/>')
    output_file.write('<span style="color:' + \
                        color_list[1] + \
                      ';font-size:14">This style is for count = 2</span><br/>')
    output_file.write('<span style="color:' + \
                        color_list[2] + \
                      ';font-size:18">This style is for count < 5</span><br/>')
    output_file.write('<span style="color:' + \
                        color_list[3] + \
                      ';font-size:22">This style is for count < max count * 0.8</span><br/>')
    output_file.write('<span style="color:' + \
                        color_list[4] + \
                      ';font-size:26">This style is for count > max count * 0.8</span>')

    # End the rest of the page                  
    output_file.write('</body>')
    output_file.write('</html>')





# This function reads the current value and writes the html code to display
# it in the popularity cloud
def write_tag(index):

    # Retrieve global functions
    global value_list, count_list, color_list
    
    output_file.write('<span style="font-size:' + \
                      str(font_size(index)) + # Font size determined in font_size \
                      'pt;color:' + \
                      str(font_color(index)) + # Font colour determined in font_color \
                      '">')
    output_file.write(str(value_list[index]) + ' [' + str(count_list[index])+ ']')
    output_file.write('</span>')

    # Create white space between tags
    output_file.write('<span style="color:rgb(255,255,255)">___</span>')
                       



    
# Determine font size the current value needs to be based on count
def font_size(index):

    # Retrieve global functions
    global count_list

    if   count_list[index] == 1:
        return 11
    elif count_list[index] == 2:
        return 13
    elif count_list[index] < 5:
        return 18
    elif count_list[index] < max(count_list) * 0.8:
        return 22
    else:
        return 26





# Determine font colour the current value needs to be based on count                      
def font_color(index):

    # Retrieve global functions
    global count_list, color_list
   
    if   count_list[index] == 1:
        return color_list[0]
    elif count_list[index] == 2:
        return color_list[1]
    elif count_list[index] < 5:
        return color_list[2]
    elif count_list[index] < max(count_list) * 0.8:
        return color_list[3]
    else:
        return color_list[4]    



##
########## END Popularity Cloud Code #############################

########## BEGIN AUTOMATIC START ###################################
##
##  The following code will automatically run the given function
##  when the program is "run".  Do not change anything in this
##  section.  If you want to prevent it from running, comment
##  out the code below, but ensure that the code is uncommented when
##  you submit your program
##

# Run the function if this is the main program:
if __name__ == '__main__':
    create_clouds(['actors', 'movies', 'tv', 'sports', 'games', \
    'activities', 'musicians', 'books'])
##
########## END AUTOMATIC START #####################################

