
########## BEGIN DESCRIPTION ############################################
##
##  Popularity Cloud.
##  This program queries a MySQL database to extract the frequencies (count) 
##  of each value in a series of categories.
##
##  The extracted data is output to a HTML "popularity" cloud, which displays
##  values in a category which have the same frequency in the same colour
##  and text size, and those which are more frequent in a larger text size.
##  Each frequency is shown in a distinct text size.
##
########## END DESCRIPTION ##############################################



########## BEGIN PAIR DETAILS #####################################
##
##  Student Name:  Sean Seeto
##
##  Student Number:  n8324018
##
##  Student Name:  Alex Crichton
##
##  Student Number:  n6878296
##
########## END PAIR DETAILS #######################################



########## BEGIN MARKER'S FEEDBACK ###################################
##
##
########## END MARKER'S FEEDBACK #####################################


########## BEGIN Popularity Code ###########################
##
##
##  Program code:
##
##  *** PUT YOUR PROGRAM CODE BELOW ***



def create_clouds(categories):
    '''
    categories - a list of strings containing the names of each of the
    categories to create HTML files for
    '''

    import MySQLdb

    # Declare these global variables for use in multiple functions later
    global output_file, value_list, count_list, color_list

    color_list = [('rgb(160,170,180)'), ('rgb(119,136,153)'), \
                  ('rgb(100,115,125)'), ('rgb(49,79,79)'), ('rgb(0,0,0)')] 

    # Create a connection using relevant values for the server name, username,
    # password and database name
    connection = MySQLdb.connect(host = "localhost", user = "root",\
    passwd = "root", db = "inb104")
    
    # Get a cursor on the database. This allows you to execute SQL queries and
    # get the results.
    cursor = connection.cursor()



    # For each of the categories entered into the create_clouds function
    for category in categories:   
            
        # Execute a SQL query
        cursor.execute("SELECT category, value, count(*) \
                        FROM popularity \
                        WHERE category = '" + category + "'\
                        GROUP BY value \
                        ORDER BY value")
        
        # Each row is a list of the column values from the database.
        rows = cursor.fetchall()

        value_list = []
        count_list = []
        
        for row in rows:
            value_list.append(row[1])
            count_list.append(row[2])
        # Create a HTML page for the current category
     
            
        

        output_file = open(category + '.html', 'w')

        write_head(category)
        for index in range(len(value_list)):
            write_tag(index, value_list, count_list, color_list)
        write_foot(color_list)
        output_file.close()

        
            






 




    
    # Close the cursor
    cursor.close()
    
    # Close the database connection
    connection.close()



                  
                      
def write_head(title):
    output_file.write('<html>')
    output_file.write('<head>')
    output_file.write('<title>')
    output_file.write(title)
    output_file.write('</title>')
    output_file.write('</head>')
    output_file.write('<body style="font-family:times">')

    output_file.write('<table border="1" bordercolor="rgb(49,79,79)" \
                       cellpadding="2"><tr><td>')
    
def write_foot(color_list):
    output_file.write('</td></tr></table>')

    output_file.write('<p style="text-align:center">')

    output_file.write('<span style="color:' + \
                        color_list[0] + \
                      ';font-size:10">This style is for count = 1</span><br/>')
    output_file.write('<span style="color:' + \
                        color_list[1] + \
                      ';font-size:14">This style is for count = 2</span><br/>')
    output_file.write('<span style="color:' + \
                        color_list[2] + \
                      ';font-size:18">This style is for count < 5</span><br/>')
    output_file.write('<span style="color:' + \
                        color_list[3] + \
                      ';font-size:22">This style is for count < max count * 0.8</span><br/>')
    output_file.write('<span style="color:' + \
                        color_list[4] + \
                      ';font-size:26">This style is for count > max count * 0.8</span>')
                      
    output_file.write('</body>')
    output_file.write('</html>')


def write_tag(index, value_list, count_list, color_list):
    output_file.write('<span style="font-size:' + \
                      str(font_size(index, count_list)) + \
                      'pt;color:' + \
                      str(font_color(index, count_list, color_list)) + \
                      '">')
    output_file.write(str(value_list[index]) + ' [' + str(count_list[index])+ ']')
    output_file.write('</span>')
    output_file.write('<span style="color:rgb(255,255,255)">___</span>')
                       

    

def font_size(index, count_list):
    max_count = max(count_list)

    if   count_list[index] == 1:
        return 11
    elif count_list[index] == 2:
        return 13
    elif count_list[index] < 5:
        return 18
    elif count_list[index] < max_count * 0.8:
        return 22
    else:
        return 26
                      
def font_color(index, count_list, color_list):

    max_count = max(count_list)
    
    if   count_list[index] == 1:
        return color_list[0]
    elif count_list[index] == 2:
        return color_list[1]
    elif count_list[index] < 5:
        return color_list[2]
    elif count_list[index] < max_count * 0.8:
        return color_list[3]
    else:
        return color_list[4]    



##
########## END Popularity Cloud Code #############################

########## BEGIN AUTOMATIC START ###################################
##
##  The following code will automatically run the given function
##  when the program is "run".  Do not change anything in this
##  section.  If you want to prevent it from running, comment
##  out the code below, but ensure that the code is uncommented when
##  you submit your program
##

# Run the function if this is the main program:
if __name__ == '__main__':
    create_clouds(['actors', 'movies', 'tv', 'sports', 'games', \
    'activities', 'musicians', 'books'])
##
########## END AUTOMATIC START #####################################

