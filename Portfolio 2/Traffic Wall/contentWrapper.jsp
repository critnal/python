<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"><!--
 /*
 * (C) Copyright Blackboard Inc. 1998-2005 - All Rights Reserved
 *
 * Permission to use, copy, modify, and distribute this software
 * without prior explicit written approval is strictly prohibited.
 * Please refer to the file "copyright.html" for further important
 * copyright and licensing information.
 *
 * BLACKBOARD MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE
 * SUITABILITY OF THE SOFTWARE, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-
 * INFRINGEMENT. BLACKBOARD SHALL NOT BE LIABLE FOR ANY DAMAGES
 * SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 */
 -->
<!-- web\content\contentWrapper.jsp -->
 <frameset  rows="40,*" id="contentFrameset" frameborder="0" framespacing="0" border="0" marginwidth="5" marginheight="0">
 <frame src="/webapps/blackboard/content/contentWrapperItem.jsp?course_id=_73578_1&content_id=_3739419_1&displayName=Project%207%20-%20Traffic%20Wall&navItem=content&attachment=true" frameborder="0" name="orientationFrame" framespacing="0" marginwidth="0" marginheight="5" scrolling="no" title="Orientation Bar" border="0" />
 <frame src="/@@/63D1FE95386749A39D7F1DD7DA2ED918/courses/1/INB104_11se1/content/_3739419_1/Project_7_Traffic_Wall.pdf" frameborder="0" name="contentFrame" framespacing="5" marginwidth="5" scrolling="auto" title="Content" border="0" target="_self" />
<noframes>
	  <body>
	  <h1>Content with Orientation Bar</h1>
	  <p>The Content frameset loads specialised content that requires the full width of the browser window, such as content from external sites or Building Blocks.</p>
		<h2>Orientation Bar</h2>
	  <p>The <a href="/webapps/blackboard/content/contentWrapperItem.jsp?course_id=_73578_1&content_id=_3739419_1&displayName=Project%207%20-%20Traffic%20Wall&navItem=content&attachment=true">Orientation Frame</a> contains links to the content hierarchy where the specialised content is located. It can be used to return to the folder containing the content or to the entry point. A link to open the full navigation menu in a new window is also available.</p>
	  <h2>Content</h2>
	  <p>The <a href="/@@/63D1FE95386749A39D7F1DD7DA2ED918/courses/1/INB104_11se1/content/_3739419_1/Project_7_Traffic_Wall.pdf">Content Frame</a> contains the specialised content, which may include content from external sites.</p>
	  </body>
  </noframes>	
 </frameset>
