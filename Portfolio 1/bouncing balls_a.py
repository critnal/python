
########## BEGIN DESCRIPTION ############################################
##
##  Bouncing Balls Animation.
##  This program gets you to bounce some balls around the screen, taking
##  care of collisions with the walls and other balls.
##
########## END DESCRIPTION ##############################################



########## BEGIN PAIR DETAILS #####################################
##
##  Student Name:  Sean Seeto
##
##  Student Number:  n8324018
##
##  Student Name:  Alex Crichton
##
##  Student Number:  n6878296
##
########## END PAIR DETAILS #######################################



########## BEGIN MARKER'S FEEDBACK ###################################
##
##
########## END MARKER'S FEEDBACK #####################################




########## BEGIN Bouncing Ball Code ###########################
##
##
##  Program code:
##

import pygame
import sys

##  *** PUT YOUR PROGRAM CODE BELOW ***
def bounce():
    # the body of bounce goes here
    ball_image = 'Beachball.jpg'
    bounce_sound = 'Thump.wav'
    width = 800
    height = 600
    background_colour = 0,0,0
    caption = 'Bouncing Ball animation'
    velocity = [1,1]


    pygame.init()

    frame = pygame.display.set_mode((width,height))
    pygame.display.set_caption(caption)

    # First ball
    ball = pygame.image.load(ball_image).convert()
    ball_boundary = ball.get_rect(center=(300,300))

    sound = pygame.mixer.Sound(bounce_sound)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit(0)

        if ball_boundary.left < 0 or ball_boundary.right > width:
            sound.play()
            velocity[0] = -1 * velocity[0]
        if ball_boundary.top < 0 or ball_boundary.bottom > height:
            sound.play()
            velocity[1] = -1 * velocity[1]

        ball_boundary = ball_boundary.move(velocity)

        frame.fill(background_colour)

        frame.blit(ball, ball_boundary)

        pygame.display.flip()

        
if __name__ =='__main__':
    bounce()
##
########## END Bouncing Ball Code #############################

