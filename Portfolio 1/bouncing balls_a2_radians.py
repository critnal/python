
########## BEGIN DESCRIPTION ############################################
##
##  Bouncing Balls Animation.
##  This program gets you to bounce some balls around the screen, taking
##  care of collisions with the walls and other balls.
##
########## END DESCRIPTION ##############################################



########## BEGIN PAIR DETAILS #####################################
##
##  Student Name:  Sean Seeto
##
##  Student Number:  n8324018
##
##  Student Name:  Alex Crichton
##
##  Student Number:  n6878296
##
########## END PAIR DETAILS #######################################



########## BEGIN MARKER'S FEEDBACK ###################################
##
##
########## END MARKER'S FEEDBACK #####################################




########## BEGIN Bouncing Ball Code ###########################
##
##
##  Program code:
##

import pygame
import sys
from random import *
import math

##  *** PUT YOUR PROGRAM CODE BELOW ***

bounce_sound = 'Thump.wav'
width = 800
height = 600
background_colour = 0,0,0
caption = 'Bouncing Ball animation'


pygame.init()

frame = pygame.display.set_mode((width,height))
pygame.display.set_caption(caption)

ball_number = []
ball_velocities = []
ball_angles = []
ball_pos = []
ball_image = pygame.image.load('beachball.jpg').convert()

def create_ball(number, speed, angle, centerx, centery) :
    
    ball_number.append(number)
    ball_velocities.append(speed)
    ball_angles.append(angle)
    ball_pos.append(ball_image.get_rect(center=(centerx,centery)))
    





def collision_ball(ball_a, ball_b) :

    v1 = ball_velocities[ball_a]
    v2 = ball_velocities[ball_b]
    

    dist_x = ball_b.left - ball_a.left    # run
    dist_y = ball_b.top  - ball_a.top     # rise

    angle1 = degrees(math.atan2(dist_y, dist_x))

    angle2 = angle1 + 180   # angle for second ball

    # vector coordinates for ball_a
    x1 = v1 * math.cos(angle1)
    y1 = v1 * math.sin(angle1)

    # vector coordinates for ball_b   
    x2 = v2 * math.cos(angle2)
    y2 = v2 * math.sin(angle2)

    # resulting cartesian vector for ball_a
    cartesian_a = y1 + x2

    # resulting cartesian vector for ball_b
    cartesian_b = y2 + x1


    # polar coordinates for ball_a
    # r = hypotenuse of x2 and y1
    # angle = tan of y1 and x2
    r_a = math.hypot(x2,y1)
    theta_a = atan2(y1,x2)
    


    # polar coordinates for ball_b
    # r = hypotenuse of x1 and y2
    # angle = tan of y2 and x1
    r_b = math.hypot(x1,y2)
    theta_b = math.atan2(y2,x1)

    # assign new vectors to respective balls
    ball_velocities[ball_a] = r_a
    ball_angles[ball_a] = theta_a

    ball_velocities[ball_b] = r_b
    ball_angles[ball_b] = theta_b

def collision_wall(ball_x, plane) :

    # find cartesian coordinates for ball
    cosx = ball_velocities[ball_x] * math.cos(ball_angles[ball_x])
    sinx = ball_velocities[ball_x] * math.sin(ball_angles[ball_x])

    # if collision with horizontal plane, reverse vertical component
    if plane == 'horizontal' :
        sinx *= -1
    
    # if collision with vertical plane, reverse horizontal component
    if plane == 'vertical' :
        cosx *= -1

    # calculate new vector
    r_x = math.hypot(cosx, sinx)
    theta_x = degrees(math.atan2(sinx, cosx))

    ball_velocities[ball_x] = r_x
    ball_angles[ball_x] = theta_x







def bounce():


    create_ball(1, 0.1, 60, randint(100, 500), randint(100, 500))
    create_ball(2, 0.2, 80, randint(100, 500), randint(100, 500))
    create_ball(3, 0.3, 30, randint(100, 500), randint(100, 500))




    sound = pygame.mixer.Sound(bounce_sound)



    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit(0)

        # Check for wall collisions

        for index in range(3) :
            
        
            if ball_pos[index].left < 0 or ball_pos[index].right > width:          
                sound.play()
                collision_wall(index, 'vertical')


            if ball_pos[index].top < 0 or ball_pos[index].bottom > height:
                sound.play()
                collision_wall(index, 'horizontal')


##        # Check for ball collision
##        if ball_pos[0].collide_rect(ball_pos[1]) :
##            collision_ball(0, 1)
##        if ball_pos[0].collide_rect(ball_pos[2]) :
##            collision_ball(0, 2)
##        if ball_pos[1].collide_rect(ball_pos[2]) :
##            collision_ball(1, 2)
        




        for index in range(3) :

            ball_pos[index].left += ball_velocities[index] * math.cos(ball_angles[index])
            ball_pos[index].top  += ball_velocities[index] * math.sin(ball_angles[index])

          
                    

       

        frame.fill(background_colour)

        for index in range(3) :
            frame.blit(ball_image, ball_pos[index])


        pygame.display.flip()

        
if __name__ =='__main__':
    bounce()
    
##
########## END Bouncing Ball Code #############################

