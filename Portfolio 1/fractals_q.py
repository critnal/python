
########## BEGIN DESCRIPTION ############################################
##
##  Fractal Drawing.
##  This program draws various fractal images using the concepts of
##  L-Systems and with Turtle Graphics.
##
########## END DESCRIPTION ##############################################



########## BEGIN PAIR DETAILS #####################################
##
##  Student Name:  *** WRITE YOUR NAME HERE***
##
##  Student Number:  *** WRITE YOUR STUDENT NUMBER HERE ***
##
##  Student Name:  *** WRITE YOUR NAME HERE***
##
##  Student Number:  *** WRITE YOUR STUDENT NUMBER HERE ***
##
########## END PAIR DETAILS #######################################



########## BEGIN MARKER'S FEEDBACK ###################################
##
##
########## END MARKER'S FEEDBACK #####################################




########## BEGIN Fractal Code ###########################
##
##
##  Algorithm:
##
##  *** PUT YOUR ALGORITHM HERE ***

##    Replace the target in the initial string with the replacement
##    Do this as many times as specified by num_replacements
##
##    Loop num_replacements number of times
##    for every target in initial_state, sub in replacement
##    then, read the final string and draw accordingly

##  Program code:
##
##  *** PUT YOUR PROGRAM CODE BELOW ***

from turtle import *

def draw_fractal(initial_state, target, replacement, num_replacements, \
                 length, angle):
    """
    This function draws a fractal using the following parameters:
       initial_state is a string
       'target' is the command to be replaced with 'replacement'
       num_replacements is an integer indicating how many times to
          the replacement rules
       length is the length to go forward
       angle is the angle to turn (either right or left)
    """
    speed('fastest')
    
    for count in range(num_replacements) :

        

        initial_state = initial_state.replace(target, replacement)
            

    for character in initial_state :

        if character == 'F' :
            forward(length)
        elif character == '-' :
            left(180 - angle)
        else :      
            right(180 - angle)

        
























    
        
##
########## END Fractal Code #############################



########## BEGIN AUTOMATIC DRAWING ###################################
##
##  The following code when uncommented will run each fractal drawing
## function
##
if __name__ == '__main__':
   
    ## Draws the Dragon_curve
    draw_fractal('F', 'F', 'F+F-F', 7, 5, 60)

    ## Draws the Koch cube
##    draw_fractal('F-F-F-F', 'F', 'FF-F-F-F-FF', 3, 3, 90)

    ## Draws the Koch ring
##    draw_fractal('F-F-F-F', 'F', 'FF-F-F-F-F-F+F', 3, 3, 90)

##
########## END AUTOMATIC TESTING #####################################
