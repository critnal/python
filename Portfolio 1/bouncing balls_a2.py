
########## BEGIN DESCRIPTION ############################################
##
##  Bouncing Balls Animation.
##  This program gets you to bounce some balls around the screen, taking
##  care of collisions with the walls and other balls.
##
########## END DESCRIPTION ##############################################



########## BEGIN PAIR DETAILS #####################################
##
##  Student Name:  Sean Seeto
##
##  Student Number:  n8324018
##
##  Student Name:  Alex Crichton
##
##  Student Number:  n6878296
##
########## END PAIR DETAILS #######################################



########## BEGIN MARKER'S FEEDBACK ###################################
##
##
########## END MARKER'S FEEDBACK #####################################




########## BEGIN Bouncing Ball Code ###########################
##
##
##  Program code:
##

import pygame
import sys
from random import *
import math

##  *** PUT YOUR PROGRAM CODE BELOW ***

bounce_sound = 'Thump.wav'
width = 800
height = 600
background_colour = 0,0,0
caption = 'Bouncing Ball animation'


pygame.init()

frame = pygame.display.set_mode((width,height))
pygame.display.set_caption(caption)

ball_number = []
ball_speedx = []
ball_speedy = []
ball_pos    = []
ball_image  = []


def create_ball(number, speedx, speedy, centerx, centery) :
    
    ball_number.append(number)
    ball_speedx.append(speedx)
    ball_speedy.append(speedy)
    
    ball_image.append(pygame.image.load('beachball.jpg').convert())
    ball_pos.append(ball_image[number-1].get_rect(center=(centerx,centery)))
    





def collision_ball(ball_a, ball_b) :


    dist_x = ball_pos[ball_b].left - ball_pos[ball_a].left    # run
    dist_y = ball_pos[ball_b].top  - ball_pos[ball_a].top     # rise

    angle1 = math.atan2(dist_y, dist_x) # rise over run
    angle2 = angle1 + math.pi   # angle for second ball

    # magnitude for each ball
    m1 = math.hypot(ball_speedx[ball_a], ball_speedy[ball_a])
    m2 = math.hypot(ball_speedx[ball_b], ball_speedy[ball_b])

    # cartesians for each ball
    x1 = m1 * math.cos(angle1)
    y1 = m1 * math.sin(angle1)

    x2 = m2 * math.cos(angle2)
    y2 = m2 * math.cos(angle2)

    # assign new vectors
    ball_speedx[ball_a] = x1
    ball_speedy[ball_a] = y2

    ball_speedx[ball_b] = x2
    ball_speedy[ball_b] = y1

##    # assign new vectors to respective balls
##    ball_speedx[ball_a] = math.hypot(ball_speedx[ball_b], ball_speedy[ball_b]) * math.cos(angle2)
##    ball_speedy[ball_a] = math.hypot(ball_speedx[ball_a], ball_speedy[ball_a]) * math.sin(angle1)
##
##    ball_speedx[ball_b] = math.hypot(ball_speedx[ball_a], ball_speedy[ball_a]) * math.cos(angle1)
##    ball_speedy[ball_b] = math.hypot(ball_speedx[ball_b], ball_speedy[ball_b]) * math.sin(angle2)

def collision_wall(ball_x, plane) :

    # if collision with horizontal plane, reverse vertical component
    if plane == 'horizontal' :
        ball_speedy[ball_x] *= -1
    
    # if collision with vertical plane, reverse horizontal component
    if plane == 'vertical' :
        ball_speedx[ball_x] *= -1






def bounce():


    create_ball(1, 1, 1, randint(100, 500), randint(100, 500))
    create_ball(1, 1, 1, randint(100, 500), randint(100, 500))
    create_ball(1, 1, 1, randint(100, 500), randint(100, 500))





    sound = pygame.mixer.Sound(bounce_sound)



    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit(0)

        # Check for wall collisions

        for index in range(len(ball_number)) :
            
        
            if ball_pos[index].left < 0 or ball_pos[index].right > width:          
                sound.play()
                collision_wall(index, 'vertical')


            if ball_pos[index].top < 0 or ball_pos[index].bottom > height:
                sound.play()
                collision_wall(index, 'horizontal')

        
        # Check for ball collisions
       
        

        if ball_pos[0].colliderect(ball_pos[1]) :
            collision_ball(0, 1)
        if ball_pos[0].colliderect(ball_pos[2]) :
            collision_ball(0, 2)
        if ball_pos[1].colliderect(ball_pos[2]) :
            collision_ball(1, 2)




        for index in range(len(ball_number)) :

            ball_pos[index].left -= ball_speedx[index]
            ball_pos[index].top  -= ball_speedy[index]

          
                    

       

        frame.fill(background_colour)

        for index in range(len(ball_number)) :
            frame.blit(ball_image[index], ball_pos[index])


        pygame.display.flip()

        
if __name__ =='__main__':
    bounce()
    
##
########## END Bouncing Ball Code #############################

