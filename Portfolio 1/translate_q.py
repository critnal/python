
########## BEGIN DESCRIPTION ############################################
##
##  French to German to English Translator.
##  Write a function named translate that takes one parameter, a French
##  phrase.  The function translates the phrase into German, and then 
##  into English again, returning the final English phrase.
## 
##  *See tests for examples of input and expected output*
##
########## END DESCRIPTION ##############################################



########## BEGIN PAIR DETAILS #####################################
##
##  Student Name:  *** WRITE YOUR NAME HERE***
## 
##  Student Number:  *** WRITE YOUR STUDENT NUMBER HERE ***
##
##  Student Name:  *** WRITE YOUR NAME HERE***
## 
##  Student Number:  *** WRITE YOUR STUDENT NUMBER HERE ***
##
########## END PAIR DETAILS #######################################



########## BEGIN MARKER'S FEEDBACK ###################################
##
##
########## END MARKER'S FEEDBACK #####################################



########## BEGIN ACCEPTANCE TESTS ####################################
##
##  This section contains the minimum tests that your program must
##  pass.  Please note, 'hard-coded' solutions are not acceptable.
##
'''
## Tests for Language Translator

>>> translate('')
... ###### Test 1 - empty string
''

>>> translate('le')
... ###### Test 2 - single word
'the'

>>> translate('le chat repose sur le natte')
... ###### Test 3
'the cat based on the mat'

>>> translate('le pluie en espagne est principalement sur le plaine')
... ###### Test 4
'the rainfall in spain is main on the even'

>>> translate('le rapide brun renard sauts au-dessus le chien paresseux')
... ###### Test 5
'the currentfast brown vulpine leap over the lazy dog'

'''

##
########## END ACCEPTANCE TESTS ######################################





########## BEGIN Translate Code ###########################
##
##
##  Algorithm:
##
##  *** PUT YOUR ALGORITHM HERE ***

##  Program code:
##
##  *** PUT YOUR PROGRAM CODE BELOW ***

# Given a phrase in French, translate it to German and then to English.
# Return the translated English phrase.
def translate(french_phrase):

    


##
########## END Translate Code #############################



########## BEGIN AUTOMATIC TESTING ###################################
##
##  The following code will automatically run the acceptance tests
##  when the program is "run".  Do not change anything in this
##  section.  If you want to prevent tests from running, comment
##  out the code below, but ensure that the code is uncommented when
##  you submit your program
##

# Run the tests if this is the main program:
if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)

##
########## END AUTOMATIC TESTING #####################################
